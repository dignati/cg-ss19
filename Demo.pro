QT += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Demo
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += c++11

release:DESTDIR = release
release:OBJECTS_DIR = release/.obj
release:MOC_DIR = release/.moc
release:RCC_DIR = release/.rcc
release:UI_DIR = release/.ui

debug:DESTDIR = debug
debug:OBJECTS_DIR = debug/.obj
debug:MOC_DIR = debug/.moc
debug:RCC_DIR = debug/.rcc
debug:UI_DIR = debug/.ui

SOURCES += \
    src/main.cpp \
    src/mainwindow.cpp \
    src/glwidget.cpp \
    src/model.cpp \
    src/skybox.cpp \
    src/screen.cpp

HEADERS += \
    src/mainwindow.h \
    src/glwidget.h \
    src/modelloader.h \
    src/model.h \
    src/skybox.h \
    src/screen.h \
    src/gauss_filter.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources/shader.qrc \
    resources/models.qrc \
    resources/textures.qrc

win32 {
    LIBS += -L../assimp-mingw32-4.1.0/bin -lassimp
    INCLUDEPATH += ../assimp-mingw32-4.1.0/include
} unix {
    CONFIG += link_pkgconfig
    PKGCONFIG += assimp
}

DISTFILES += \
    resources/shaders/screen.vert \
    resources/shaders/screen.frag \
    resources/shaders/post.comp
