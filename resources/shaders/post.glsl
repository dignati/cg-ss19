#version 450 core

layout(local_size_x = 1, local_size_y = 1) in;
layout(rgba8, binding = 0) readonly restrict uniform image2D colorTex;
layout(rgba8, binding = 1) writeonly restrict uniform image2D postTex;
uniform float coeff[30];
uniform int filterSize;

vec4 horizontal(vec4 color) {
    ivec2 pos = ivec2(gl_GlobalInvocationID.xy);
    int offset = (filterSize - 1) / 2;
    pos.x -= offset;

    for(int i = 0; i <= filterSize; i++) {
        color += coeff[i] * imageLoad(colorTex, pos);
        pos.x += 1;
    }
    return color;
}
vec4 vertical(vec4 color) {
    ivec2 pos = ivec2(gl_GlobalInvocationID.xy);
    int offset = (filterSize - 1) / 2;
    pos.y -= offset;

    color *= coeff[offset];
    for(int i = 0; i <= filterSize; i++) {
        if(i != offset) {
            color += coeff[i] * imageLoad(colorTex, pos);
            pos.y += 1;
        }
    }
    return color;
}

void main(void)
{
 ivec2 pos = ivec2(gl_GlobalInvocationID.xy);
 vec4 color = vec4(0.0);
 color = horizontal(color);
 color = vertical(color);
 imageStore(postTex, pos, color);
}
