#pragma once
#include <qopengl.h>
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_4_5_Core>

class SkyBox: public QOpenGLFunctions_4_5_Core {
 public:
  SkyBox();
  ~SkyBox();
  void paint(const QMatrix4x4& projection, QMatrix4x4 view);

 private:
  GLuint texture;
  GLuint vao;
  GLuint vbo;
  GLuint ibo;
  QOpenGLShaderProgram* program;
};
