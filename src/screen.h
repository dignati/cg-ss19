#pragma once
#include <QOpenGLFunctions_4_5_Core>
#include <QOpenGLShaderProgram>

class Screen: public QOpenGLFunctions_4_5_Core
{
public:
    Screen();
    void paint();
    void init();
private:
    QOpenGLShaderProgram* program;
    GLuint vao;
    GLuint vbo;
    GLuint ibo;

};
