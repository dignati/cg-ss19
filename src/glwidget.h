#pragma once

#include <QElapsedTimer>
#include <QObject>
#include <QOpenGLDebugLogger>
#include <QOpenGLFunctions_4_5_Core>
#include <QOpenGLShaderProgram>
#include <QOpenGLWidget>
#include <QVector3D>
#include <QWidget>

#include "model.h"
#include "skybox.h"
#include "screen.h"

#define NUM_LS 5

struct LightSource {
  QVector3D position;
  float _pad0;

  QVector3D color;
  float _pad1;

  float ka;
  float kd;
  float ks;
  float _pad2;

  float constant;
  float linear;
  float quadratic;
  float _pad3;
};

class GLWidget : public QOpenGLWidget, QOpenGLFunctions_4_5_Core {
  Q_OBJECT

 public:
  GLWidget(QWidget* parent) : QOpenGLWidget(parent) {
    setFocusPolicy(Qt::StrongFocus);
  }
  ~GLWidget() {
    makeCurrent();
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ibo);
    glDeleteVertexArrays(1, &vao);
    glDeleteTextures(1, &texture);
  }

 private:
  int fov = 45;
  int angle = 0;
  int filterSize = 3;
  float coeff[50];
  double near = 0.0;
  double far = 0.0;
  double rotationA = 0.0;
  double rotationB = 0.0;
  double rotationC = 0.0;
  bool perspectiveProjection = true;
  bool mountCamera = false;
  bool renderDepth = false;
  const double MIN_CLIPPING_DISTANCE = 2.0;
  QVector3D cameraPosition = QVector3D(0, 0, -3);
  QVector3D cameraCenter;

  GLfloat alpha = 0.0f;
  GLfloat shiftU = 0;
  QMatrix4x4 rotMat;

  GLuint vao;
  GLuint vbo;
  GLuint ibo;
  GLuint fbo;
  GLuint post_fbo;
  GLuint depthTex;
  GLuint post_depthTex;
  GLuint colorTex;
  GLuint post_colorTex;
  GLuint texture;
  QOpenGLShaderProgram* program;
  QOpenGLShaderProgram* lightProgram;
  QOpenGLShaderProgram* computeProgram;

  Model* gimbal;
  Model* sphere;
  Model* light;

  SkyBox* skybox;

  Screen* screen;

  LightSource lightSources[NUM_LS];
  unsigned int uboLights;

  QElapsedTimer* timer;

  QOpenGLDebugLogger* debugLogger;

 public slots:
  void setFov(int value);
  void setAngle(int value);
  void setProjectionMode();
  void setNear(double value);
  void setFar(double value);
  void setRotationA(double value);
  void setRotationB(double value);
  void setRotationC(double value);
  void onMessageLogged(QOpenGLDebugMessage message);

 protected:
  void keyPressEvent(QKeyEvent* event);

 signals:
  void keepNearDistance(double newNear);
  void keepFarDistance(double newFar);

  // QOpenGLWidget interface
 protected:
  void initializeGL();
  void resizeGL(int w, int h);
  void paintGL();
};
