#include "model.h"
#include <iostream>

Model::Model(const QString& model_name) : loader() {
  initializeOpenGLFunctions();
  loader.loadObjectFromFile(model_name);
  vbo_len = loader.lengthOfVBO();
  vbo_data = new GLfloat[vbo_len];
  loader.genVBO(vbo_data);
  ibo_len = loader.lengthOfIndexArray();
  ibo_data = new GLuint[ibo_len];
  loader.genIndexArray(ibo_data);
  transform = new QMatrix4x4();
}

Model::~Model() {
  glDeleteBuffers(1, &vbo);
  glDeleteBuffers(1, &ibo);
  glDeleteVertexArrays(1, &vao);
}

void Model::reset() { this->transform = new QMatrix4x4(); }
void Model::rotate(float angle, float x, float y, float z) {
  this->transform->rotate(angle, x, y, z);
}
void Model::translate(float x, float y, float z) {
  this->transform->translate(x, y, z);
}

void Model::scale(float s) { this->transform->scale(s); }

void Model::initialize(QOpenGLShaderProgram* prog) {
  this->program = prog;

  // Bind VAO
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // Bind VBO
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, vbo_len * sizeof(vbo_data[0]), vbo_data,
               GL_STATIC_DRAW);

  // Bind IBO
  glGenBuffers(1, &ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibo_len * sizeof(ibo_data[0]), ibo_data,
               GL_STATIC_DRAW);

  // Define VAO
  glEnableVertexAttribArray(0);  // position
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float[8]), 0);
  glEnableVertexAttribArray(1);  // normal
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float[8]),
                        (void*)sizeof(float[3]));
  glEnableVertexAttribArray(2);  // texture coordinate
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(float[8]),
                        (void*)sizeof(float[6]));
}

void Model::paint() {
  if (program != nullptr) {
    program->setUniformValue("model", *transform);
    program->setUniformValue("normalMat", transform->normalMatrix());
    program->setUniformValue("material.ambient", material.ambient);
    program->setUniformValue("material.diffuse", material.diffuse);
    program->setUniformValue("material.specular", material.specular);
    program->setUniformValue("material.shininess", material.shininess);
  }
  glBindVertexArray(vao);
  glDrawElements(GL_TRIANGLES, ibo_len, GL_UNSIGNED_INT, nullptr);
}
