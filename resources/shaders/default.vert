#version 450 core

uniform mat4x4 model;
uniform mat4x4 view;
uniform mat4x4 projection;
uniform mat3x3 normalMat;

layout(location = 0) in vec3 attrPosition;
layout(location = 1) in vec3 attrNormal;
layout(location = 2) in vec2 attrUV;

layout(location = 0) out vec2 vertexUV;
layout(location = 1) out vec3 normal;
layout(location = 2) out vec3 fragPosition;

void main () {
    vertexUV = attrUV;
    normal = normalize(normalMat * attrNormal);
    fragPosition = (model * vec4(attrPosition, 1.0)).xyz;
    gl_Position = projection * view * model * vec4(attrPosition, 1.0);
}
