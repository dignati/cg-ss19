#include "glwidget.h"
#include <QtWidgets>
#include <iostream>
#include "gauss_filter.h"

void GLWidget::keyPressEvent(QKeyEvent* event) {
  const auto key = event->key();
  const auto up = QVector3D(0, 1, 0);
  auto f = (cameraCenter - cameraPosition);
  f.normalize();

  auto s = QVector3D::crossProduct(f, up);
  s.normalize();

  auto u = QVector3D::crossProduct(s, f);

  s *= 0.2;
  u *= 0.1;

  if (key == Qt::Key_A) {
    cameraPosition -= s;
    cameraCenter -= s;
  } else if (key == Qt::Key_D) {
    cameraPosition += s;
    cameraCenter += s;
  } else if (key == Qt::Key_W) {
    cameraPosition += f;
    cameraCenter += f;
  } else if (key == Qt::Key_S) {
    cameraPosition -= f;
    cameraCenter -= f;
  }

  if (key == Qt::Key_Left) {
    cameraCenter -= s;
  } else if (key == Qt::Key_Right) {
    cameraCenter += s;
  } else if (key == Qt::Key_Up) {
    cameraCenter += u;
  } else if (key == Qt::Key_Down) {
    cameraCenter -= u;
  }

  if (key == Qt::Key_M) {
      renderDepth = !renderDepth;
  }

  if (key == Qt::Key_Space) {
      QImage img (size(), QImage::Format_RGBA8888 );
      // make/doneCurrent() are only needed when calling from outside paintGL()
      makeCurrent();
      glBindTexture(GL_TEXTURE_2D, colorTex);
      glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, img.bits());
      doneCurrent();
      img.mirrored(false, true).save("my_filename.png");
  }

  std::cout << "cameraPostion(" << cameraPosition.x() << ", "
            << cameraPosition.y() << ", " << cameraPosition.z() << ");"
            << std::endl;
}

void GLWidget::setFov(int value) { fov = value; }

void GLWidget::setAngle(int value) {
    if(value % 2 == 1) {
        filterSize = value;
        auto coeff_d = generate1DGauss(filterSize);
        for(int i = 0; i < filterSize; i++) {
            coeff[i] = coeff_d[i];
        }
    }
}

void GLWidget::setProjectionMode() {
  perspectiveProjection = !perspectiveProjection;
  mountCamera = !mountCamera;
  std::cout << "Is perspective: " << perspectiveProjection << std::endl;
}

void GLWidget::setNear(double value) {
  near = value;
  if (far - near < MIN_CLIPPING_DISTANCE) {
    emit keepFarDistance(near + 2.0);
  }
}

void GLWidget::setFar(double value) {
  far = value;
  if (far - near < MIN_CLIPPING_DISTANCE) {
    emit keepNearDistance(far - 2.0);
  }
}

void GLWidget::setRotationA(double value) {
  alpha = value / 100;
  rotationA = value;
}

void GLWidget::setRotationB(double value) {
  shiftU = (value - 50) / 50;
  rotationB = value;
}

void GLWidget::setRotationC(double value) {
  QMatrix4x4 newRotation;
  newRotation.rotate(value, QVector3D(0, 1, 0));
  rotMat = newRotation;
  rotationC = value;
}

void GLWidget::onMessageLogged(QOpenGLDebugMessage message) {
  qDebug() << message;
}

void GLWidget::initializeGL() {

  // Debugger
  debugLogger = new QOpenGLDebugLogger(this);
  connect(debugLogger, &QOpenGLDebugLogger::messageLogged, this,
          &GLWidget::onMessageLogged);
  if (debugLogger->initialize()) {
    debugLogger->startLogging(QOpenGLDebugLogger::SynchronousLogging);
    debugLogger->enableMessages();
  }

  Q_ASSERT(initializeOpenGLFunctions());


  // OpenGL Config
  glClearColor(0.3, 0.3, 0.3, 1);
  glEnable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


  // Post-Processing FBO
  glGenFramebuffers(1, &post_fbo);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER , post_fbo);
  glGenTextures(1, &post_colorTex);
  glBindTexture(GL_TEXTURE_2D, post_colorTex);
  glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width(), height());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, post_colorTex, 0);
  glGenTextures(1, &post_depthTex);
  glBindTexture(GL_TEXTURE_2D, post_depthTex);
  glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT16, width(), height());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, post_depthTex, 0);
  Q_ASSERT(glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

  // Off-Screen-Rendering FBO
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER , fbo);
  glGenTextures(1, &colorTex);
  glBindTexture(GL_TEXTURE_2D, colorTex);
  glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width(), height());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTex, 0);
  glGenTextures(1, &depthTex);
  glBindTexture(GL_TEXTURE_2D, depthTex);
  glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT16, width(), height());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTex, 0);
  Q_ASSERT(glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

  glActiveTexture(GL_TEXTURE0);

  // Shaders
  program = new QOpenGLShaderProgram();
  program->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/default.vert");
  program->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/texture.frag");
  Q_ASSERT(program->link());

  lightProgram = new QOpenGLShaderProgram();
  lightProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/default.vert");
  lightProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/color.frag");
  Q_ASSERT(lightProgram->link());

  computeProgram = new QOpenGLShaderProgram();
  computeProgram->addShaderFromSourceFile(QOpenGLShader::Compute, ":/shaders/post.glsl");
  Q_ASSERT(computeProgram->link());

  // Objects
  gimbal = new Model(":/models/gimbal.obj");
  gimbal->initialize(program);
  gimbal->material.ambient = QVector3D(0.2125, 0.1275, 0.054);
  gimbal->material.diffuse = QVector3D(0.714, 0.4284, 0.18144);
  gimbal->material.specular = QVector3D(0.393548, 0.271906, 0.166721);
  gimbal->material.shininess = 0.2 * 128;
  sphere = new Model(":/models/sphere.obj");
  sphere->initialize(program);
  sphere->material.ambient = QVector3D(0.25, 0.25, 0.25);
  sphere->material.diffuse = QVector3D(0.4, 0.4, 0.4);
  sphere->material.specular = QVector3D(0.774597, 0.774597, 0.774597);
  sphere->material.shininess = 0.6;

  // Lights
  for (int i = 0; i < NUM_LS; i++) {
    lightSources[i].position = QVector3D((i + 1) % 2, i % 3, i % 4);
    lightSources[i].color = QVector3D(i % 2, i % 3, i % 4);
    lightSources[i].ka = 0.8;
    lightSources[i].kd = 0.8;
    lightSources[i].ks = 1.0f;
    lightSources[i].constant = 1;
    lightSources[i].linear = 0.022;
    lightSources[i].quadratic = 0.0020;
  }

  glGenBuffers(1, &uboLights);
  glBindBuffer(GL_UNIFORM_BUFFER, uboLights);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(lightSources), &lightSources, GL_DYNAMIC_DRAW);
  glBindBufferBase(GL_UNIFORM_BUFFER, 0, uboLights);
  glBindBuffer(GL_UNIFORM_BUFFER, 0);

  // Skybox
  skybox = new SkyBox();

  // Screen
  screen = new Screen();
  screen->init();

  // Textures
  QImage texImg;
  texImg.load(":/textures/gimbal_wood.jpg");
  Q_ASSERT(!texImg.isNull());
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, texImg.width(), texImg.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, texImg.bits());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  // Animation timer
  timer = new QElapsedTimer();
  timer->start();

  // Blur
  auto coeff_d = generate1DGauss(filterSize);
  for(int i = 0; i < filterSize; i++) {
      coeff[i] = coeff_d[i];
  }

}

void GLWidget::resizeGL(int w, int h) {}

void GLWidget::paintGL() {

  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);
  program->bind();

  glBindBuffer(GL_UNIFORM_BUFFER, uboLights);
  glBindBufferBase(GL_UNIFORM_BUFFER, 0, uboLights);
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(lightSources), &lightSources);

  qint64 elapsed = timer->elapsed();
  float changeA = sin((elapsed / 1500.0));
  float changeB = cos((elapsed / 1300.0));
  float changeC = sin((elapsed / 1800.0));
  rotationA = changeA * 180.0;
  rotationB = changeB * 200.0;
  rotationC = changeC * 190.0;

  QMatrix4x4 projection = QMatrix4x4();
  projection.perspective(fov, 1, 0.1, 100.0);

  QMatrix4x4 view = QMatrix4x4();
  QVector3D viewPosition = cameraPosition;
  QVector3D viewCenter = cameraCenter;

  if (mountCamera) {
    view.rotate(rotationA, 1, 0, 0);
    view.rotate(rotationB, 0, 1, 0);
    view.rotate(rotationC, 1, 0, 0);
  } else {
    view.lookAt(viewPosition, viewCenter, QVector3D(0, 1, 0));
  }

  QMatrix4x4 lightTransform = QMatrix4x4();
  lightTransform.rotate(changeA, 1, 0, 0);
  lightTransform.rotate(changeB, 0, 1, 0);
  lightTransform.rotate(changeC, 0, 0, 1);

  for (int i = 0; i < NUM_LS; i++) {
    lightSources[i].position = lightTransform * lightSources[i].position;
  };

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);

  program->setUniformValue("viewPosition", viewPosition);
  program->setUniformValue("view", view);
  program->setUniformValue("projection", projection);
  program->setUniformValue("tex", 0);
  program->setUniformValue("cubeMap", 1);

  gimbal->reset();
  // Outer Gimbal
  gimbal->rotate(rotationA, 1, 0, 0);
  gimbal->paint();
  // Middle Gimbal
  gimbal->scale(0.7);
  gimbal->rotate(rotationB, 0, 1, 0);
  gimbal->paint();
  // Inner Gimbal
  gimbal->scale(0.7);
  gimbal->rotate(rotationC, 1, 0, 0);
  gimbal->paint();

  // Rotating sphere
  sphere->reset();
  sphere->reprogram(program);
  sphere->rotate(rotationA, 1, 0, 0);
  sphere->rotate(rotationB, 0, 1, 0);
  sphere->rotate(angle, 0, 0, 1);
  sphere->translate(0, 0.8, 0);
  sphere->scale(0.05);
  sphere->paint();

  program->release();
  // Lights
  lightProgram->bind();
  lightProgram->setUniformValue("viewPosition", viewPosition);
  lightProgram->setUniformValue("view", view);
  lightProgram->setUniformValue("projection", projection);

  sphere->reprogram(lightProgram);
  for (int i = 0; i < NUM_LS; i++) {
    auto lightPosition = lightSources[i].position;
    sphere->reset();
    sphere->translate(lightPosition.x(), lightPosition.y(), lightPosition.z());
    sphere->scale(0.05);
    sphere->paint();
  }
  lightProgram->release();

  skybox->paint(projection, view);

  glBindImageTexture(0, colorTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
  glBindImageTexture(1, post_colorTex, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);

  computeProgram->bind();
  computeProgram->setUniformValue("filterSize", filterSize);
  computeProgram->setUniformValueArray("coeff", coeff, filterSize, 1);

  glDispatchCompute(width(), height(), 1);
  glBindFramebuffer(GL_READ_FRAMEBUFFER, post_fbo);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, defaultFramebufferObject());

  glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
  glBlitFramebuffer(0, 0, width(), height(), 0, 0, width(), height(), GL_COLOR_BUFFER_BIT, GL_NEAREST);
  computeProgram->release();

  if(renderDepth) {
      glActiveTexture(GL_TEXTURE3);
      glBindTexture(GL_TEXTURE_2D, depthTex);
      screen->paint();
      glBindTexture(GL_TEXTURE_2D, 0);
  }

  glReadBuffer(GL_COLOR_ATTACHMENT0);
  update();
}
