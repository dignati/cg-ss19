#version 450 core

layout(location = 0) out vec4 fragmentColor;
in vec3 normal;

void main () {
    fragmentColor = vec4(0.2, 0.2, 0.2, 1);
}
