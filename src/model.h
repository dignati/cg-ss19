#pragma once
#include <QOpenGLShaderProgram>
#include <string>
#include "modelloader.h"
#include <QOpenGLFunctions_4_5_Core>

struct Material {
  QVector3D ambient;
  QVector3D diffuse;
  QVector3D specular;
  float shininess;
};

class Model: public QOpenGLFunctions_4_5_Core {
 public:
  Model(const QString& model_name);
  ~Model();
  Material material;
  void initialize(QOpenGLShaderProgram* prog);
  void paint();
  void reprogram(QOpenGLShaderProgram* prog) { this->program = prog; };
  void reset();
  void rotate(float angle, float x, float y, float z);
  void translate(float x, float y, float z);

  void scale(float s);

 private:
  QOpenGLShaderProgram* program;
  QMatrix4x4* transform;
  ModelLoader loader;
  GLuint vao;
  GLuint vbo;
  GLuint ibo;
  GLuint vbo_len;
  GLuint ibo_len;
  GLfloat* vbo_data;
  GLuint* ibo_data;
};
