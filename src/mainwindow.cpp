#include "mainwindow.h"
#include <iostream>
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  reset();

  // Signals to GLWidget
  QObject::connect(ui->fovSlider, &QSlider::valueChanged, ui->openGLWidget,
                   &GLWidget::setFov);
  QObject::connect(ui->angleSlider, &QSlider::valueChanged, ui->openGLWidget,
                   &GLWidget::setAngle);
  QObject::connect(ui->perspectiveButton, &QRadioButton::toggled,
                   ui->openGLWidget, &GLWidget::setProjectionMode);
  QObject::connect(ui->clippingNearSpinner,
                   QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                   ui->openGLWidget, &GLWidget::setNear);
  QObject::connect(ui->clippingFarSpinner,
                   QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                   ui->openGLWidget, &GLWidget::setFar);
  QObject::connect(ui->ringASlider, &QSlider::valueChanged, ui->openGLWidget,
                   &GLWidget::setRotationA);
  QObject::connect(ui->ringBSlider, &QSlider::valueChanged, ui->openGLWidget,
                   &GLWidget::setRotationB);
  QObject::connect(ui->ringCSlider, &QSlider::valueChanged, ui->openGLWidget,
                   &GLWidget::setRotationC);

  QObject::connect(ui->openGLWidget, &GLWidget::keepNearDistance,
                   ui->clippingNearSpinner, &QDoubleSpinBox::setValue);
  QObject::connect(ui->openGLWidget, &GLWidget::keepFarDistance,
                   ui->clippingFarSpinner, &QDoubleSpinBox::setValue);

  QObject::connect(ui->resetButton, &QPushButton::clicked, [&]() { reset(); });

  // Logging signals
  QObject::connect(ui->fovSlider, &QSlider::valueChanged, [](int value) {
    std::cout << "FOV: " << value << std::endl;
  });
  QObject::connect(ui->angleSlider, &QSlider::valueChanged, [](int value) {
    std::cout << "Angle: " << value << std::endl;
  });
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::reset() {
  ui->fovSlider->setValue(45);
  ui->angleSlider->setValue(0);
  ui->perspectiveButton->setChecked(true);
  ui->clippingNearSpinner->setValue(0);
  ui->clippingFarSpinner->setValue(0);
  ui->ringASlider->setValue(0);
  ui->ringBSlider->setValue(0);
  ui->ringCSlider->setValue(0);
}
