#version 450 core

layout (location = 0) in vec2 vertexPosition;
layout (location = 0) out vec2 fragmentPosition;

void main(void)
{
    fragmentPosition = 0.5* (vertexPosition + vec2(1.0));
    gl_Position = vec4((0.3 * vertexPosition) - vec2(0.5), -1.0, 1.0);
}
