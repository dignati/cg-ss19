#version 450 core

layout (location = 0) in vec2 fragmentPosition;
layout(location = 0) out vec4 fragmentColor;
uniform sampler2D tex;

void main(void)
{
    fragmentColor = vec4(vec3(pow(texture(tex, fragmentPosition).r, 12)), 1.0);
}
