#include "screen.h"

Screen::Screen()
{
    initializeOpenGLFunctions();
}

void Screen::init() {


    static const GLfloat vbo_data[] = {-1, -1,
                                       1, -1,
                                      -1,  1,
                                       1,  1,};
    static const GLuint ibo_data[] = {0, 1, 2, 1, 2, 3};

    // Bind VAO
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    // Bind VBO
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vbo_data), vbo_data, GL_STATIC_DRAW);


    // Bind IBO
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(ibo_data), ibo_data, GL_STATIC_DRAW);

    // Define VAO
    glEnableVertexAttribArray(0);  // position
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float[2]), 0);

    // Link shader program
    program = new QOpenGLShaderProgram();
    program->addShaderFromSourceFile(QOpenGLShader::Vertex,
                                     ":/shaders/screen.vert");
    program->addShaderFromSourceFile(QOpenGLShader::Fragment,
                                     ":/shaders/screen.frag");
    Q_ASSERT(program->link());
}

void Screen::paint() {
    program->bind();
    program->setUniformValue("tex", 3);
    glBindVertexArray(vao);
    glDisable(GL_CULL_FACE);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
    glEnable(GL_CULL_FACE);
    program->release();
}
