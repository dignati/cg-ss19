#include "skybox.h"
#include <QImage>
#include <QMatrix4x4>
#include <QOpenGLShader>
#include <iostream>

SkyBox::SkyBox() {
  initializeOpenGLFunctions();
  QImage rightImage;
  rightImage.load(":/textures/skybox/right.jpg");
  QImage leftImage;
  leftImage.load(":/textures/skybox/left.jpg");
  QImage topImage;
  topImage.load(":/textures/skybox/top.jpg");
  QImage bottomImage;
  bottomImage.load(":/textures/skybox/bottom.jpg");
  QImage backImage;
  backImage.load(":/textures/skybox/back.jpg");
  QImage frontImage;
  frontImage.load(":/textures/skybox/front.jpg");

  Q_ASSERT(!rightImage.isNull());
  Q_ASSERT(!leftImage.isNull());
  Q_ASSERT(!topImage.isNull());
  Q_ASSERT(!bottomImage.isNull());
  Q_ASSERT(!backImage.isNull());
  Q_ASSERT(!frontImage.isNull());

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, rightImage.width(),
               rightImage.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE,
               reinterpret_cast<void const*>(rightImage.constBits()));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, leftImage.width(),
               leftImage.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE,
               reinterpret_cast<void const*>(leftImage.constBits()));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, topImage.width(),
               topImage.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE,
               reinterpret_cast<void const*>(topImage.constBits()));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, bottomImage.width(),
               bottomImage.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE,
               reinterpret_cast<void const*>(bottomImage.constBits()));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, backImage.width(),
               backImage.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE,
               reinterpret_cast<void const*>(backImage.constBits()));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, frontImage.width(),
               frontImage.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE,
               reinterpret_cast<void const*>(frontImage.constBits()));
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  static const GLfloat vbo_data[] = {
      -1, 1,  -1, -1, -1, -1, 1, -1, -1, 1, 1, -1,
      -1, -1, 1,  -1, 1,  1,  1, -1, 1,  1, 1, 1,
  };

  static const GLuint ibo_data[] = {
      0, 1, 2, 2, 3, 0, 4, 1, 0, 0, 5, 4, 2, 6, 7, 7, 3, 2,
      4, 5, 7, 7, 6, 4, 0, 3, 7, 7, 5, 0, 1, 4, 2, 2, 4, 6,
  };

  // Bind VAO
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // Bind VBO
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vbo_data), vbo_data, GL_STATIC_DRAW);

  // Bind IBO
  glGenBuffers(1, &ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(ibo_data), ibo_data, GL_STATIC_DRAW);

  // Define VAO
  glEnableVertexAttribArray(0);  // position
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float[3]), 0);

  // Link shader program
  program = new QOpenGLShaderProgram();
  program->addShaderFromSourceFile(QOpenGLShader::Vertex,
                                   ":/shaders/skybox.vert");
  program->addShaderFromSourceFile(QOpenGLShader::Fragment,
                                   ":/shaders/skybox.frag");
  Q_ASSERT(program->link());
}

void SkyBox::paint(const QMatrix4x4& projection, QMatrix4x4 view) {
  // temporarily disable writing depth values
  glDepthMask(GL_FALSE);
  // Erase translation in view matrix
  view.column(3) = QVector4D(0.0, 0.0, 0.0, 0.0);
  view.scale(10);
  // Bind VAO, shader, cube texture
  glBindVertexArray(vao);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
  // Bind Program and fill uniforms
  program->bind();
  program->setUniformValue("uProjection", projection);
  program->setUniformValue("uView", view);
  program->setUniformValue("uSkybox", 1);

  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
  // Release VAO
  glBindVertexArray(0);
  // Turn depth writes back on
   glDepthMask(GL_TRUE);
}

SkyBox::~SkyBox() {
  glDeleteTextures(1, &texture);
  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1, &vbo);
  glDeleteBuffers(1, &ibo);
}
