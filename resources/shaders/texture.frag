#version 450 core
#define NUM_LS 5

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct PointLight {
    vec3 position;
    vec3 color;
    float ambient;
    float diffuse;
    float specular;
    float constant;
    float linear;
    float quadratic;
};

layout (std140) uniform lightBlock {
    PointLight lights[NUM_LS];
};

uniform vec3 viewPosition;
uniform Material material;

uniform sampler2D tex;
uniform samplerCube uSkybox;

in vec2 vertexUV;
in vec3 normal;
in vec3 fragPosition;

layout(location = 0) out vec4 fragmentColor;

vec3 calcPhongLight(PointLight light, vec3 viewDirection, vec3 lightDirection) {
    vec3 R = reflect(-lightDirection, normal);
    float d = distance(fragPosition, light.position);

    vec3 ambient = light.ambient * material.ambient;
    vec3 diffuse = light.diffuse * material.diffuse * max(0.0, dot(lightDirection, normal));
    vec3 specular = light.specular * material.specular * max(0.0, pow(dot(viewDirection, R), material.shininess));
    float attenuation = 1.0 / (light.constant + light.linear*d + light.quadratic*d*d);

    return 3 * light.color * (ambient + diffuse + specular);
}

void main () {
    vec3 viewDirection = normalize(viewPosition - fragPosition);
    vec3 lighting = vec3(0.0);

    for(int i = 0; i < NUM_LS; i++){
        PointLight light = lights[i];
        vec3 lightDirection = normalize(light.position - fragPosition);
        lighting += calcPhongLight(light, viewDirection, lightDirection);
    }

    vec3 R = reflect(-viewDirection, normal);
    vec4 texel = texture(uSkybox, R);
    // vec4 texel = texture(tex, vertexUV);
    fragmentColor = texel * vec4(lighting, 1.0);
}
